# Oceanish Theme

Oceanish color theme for Code-OSS based apps (VSCode, Codium, code.dev, AzureDataStudio, TheiaIDE and Positron). Like Matrixish, but Aqua.

Created by sjsepan.

**Enjoy!**

VSCode:
![./images/sjsepan-oceanish_code.png](./images/sjsepan-oceanish_code.png?raw=true "VSCode")
Codium:
![./images/sjsepan-oceanish_codium.png](./images/sjsepan-oceanish_codium.png?raw=true "Codium")
Code.Dev:
![./images/sjsepan-oceanish_codedev.png](./images/sjsepan-oceanish_codedev.png?raw=true "Code.Dev")
Azure Data Studio:
![./images/sjsepan-oceanish_ads.png](./images/sjsepan-oceanish_ads.png?raw=true "Azure Data Studio")
TheiaIDE:
![./images/sjsepan-oceanish_theia.png](./images/sjsepan-oceanish_theia.png?raw=true "TheiaIDE")
Positron:
![./images/sjsepan-oceanish_positron.png](./images/sjsepan-oceanish_positron.png?raw=true "Positron")

## Contact

Steve Sepan

<sjsepan@yahoo.com>

1/10/2025
